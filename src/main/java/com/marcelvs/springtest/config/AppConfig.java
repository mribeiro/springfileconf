package main.java.com.marcelvs.springtest.config;

import main.java.com.marcelvs.springtest.core.WiredClass;
import main.java.com.marcelvs.springtest.equipment.Card;
import main.java.com.marcelvs.springtest.equipment.Device;
import main.java.com.marcelvs.springtest.equipment.Port;
import main.java.com.marcelvs.springtest.equipment.Slot;
import main.java.com.marcelvs.springtest.shelf.Rack;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

 
@Configuration
public class AppConfig {
    
    @Bean(name="portBean")
    public Port getPort() {
        return new Port();
    }
    
    @Bean(name="cardBean")
    public Card getCard(Port port) {
        return new Card(port);
    } 
    
    @Bean(name="slotBean")
    public Slot getSlot(Card card) {
        return new Slot(card);
    } 
 
    @Bean(name="deviceBean")
    public Device getDevice(Slot slot) {
        return new Device(slot);
    } 
    
    @Bean(name="rackBean")
    public Rack getRack(Device device) {
        return new Rack(device);
    } 
    
    @Bean(name="wiredClassBean")
    public WiredClass getWiredClass() {
        return new WiredClass();
    } 
    
}
