package main.java.com.marcelvs.springtest.equipment;

public class Card {

    private final Port port;
    
    public Card(Port port) {
        this.port = port;
    }
    
    @Override
    public String toString() {
        return "My card number is: I really don't care!\n" + port;
    }
    
}
