package main.java.com.marcelvs.springtest.equipment;

public class Port {
    
    private final int portNo;
    
    public Port() {
        this.portNo = 232;
    }
    
    @Override
    public String toString() {
        return "My port number is " + portNo;
    }

}
