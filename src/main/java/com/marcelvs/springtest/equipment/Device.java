package main.java.com.marcelvs.springtest.equipment;

public class Device {
    private final Slot slot;
    
    public Device(Slot slot) {
        this.slot = slot;
    }

    @Override
    public String toString() {
        return "I am a device, I really have something to say, so...!\n" + slot;
    }
}
