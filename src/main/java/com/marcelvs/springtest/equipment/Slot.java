package main.java.com.marcelvs.springtest.equipment;

public class Slot {
    
    private final Card card;
    
    public Slot(Card card) {
        this.card = card;
    }
    
    @Override
    public String toString() {
        return "My Slot number is: I really don't care!\n" + card;
    }
}
