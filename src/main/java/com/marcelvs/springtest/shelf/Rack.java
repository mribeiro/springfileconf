package main.java.com.marcelvs.springtest.shelf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.java.com.marcelvs.springtest.equipment.Device;

@Component
public class Rack {
    Device device;
    
    @Autowired
    public Rack(Device device) {
        this.device = device;
    }
    
    @Override
    public String toString() {
        return "I have the following device:\n" + device;
    }
}
