package main.java.com.marcelvs.springtest.core;

import main.java.com.marcelvs.springtest.config.AppConfig;
import main.java.com.marcelvs.springtest.equipment.Device;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
// Comentario trouxa 
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
//        
//        Device device = (Device) context.getBean("deviceBean");
        
//        System.out.println(device);
        
        WiredClass wiredClass = (WiredClass) context.getBean("wiredClassBean");
        
        System.out.println("Now injecting this device in another bean");
        wiredClass.printWiredInformaition();
 
    }
}
