package main.java.com.marcelvs.springtest.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import main.java.com.marcelvs.springtest.shelf.Rack;

@Component
public class WiredClass {

    @Autowired
    private Rack rack;
    
    public void printWiredInformaition() {
        System.out.println("This information was extracted from my brand new rack!");
        System.out.println(rack.toString());
    }
    
}
